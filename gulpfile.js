'use strict'

const config = {
    /**
     * If the development environment
     */
    env_dev: true,

    /**
     * Source path
     */
    src: 'src',

    /**
     * Dist path
     */
    dist: 'dist'
}

/**
 * Requires
 */
const gulp = require('gulp')
const browserSync = require('browser-sync').create()
const reload = browserSync.reload
const sass = require('gulp-sass')
const autoprefixer = require('gulp-autoprefixer')
const replace = require('gulp-replace')
const sourcemaps = require('gulp-sourcemaps')
const useref = require('gulp-useref')
const _if = require('gulp-if')
const uglify = require('gulp-uglify')
const cssnano = require('gulp-cssnano')
const imagemin = require('gulp-imagemin')
const runSequence = require('run-sequence')
const del = require('del')
const flatten = require('gulp-flatten')
const webpackStream = require('webpack-stream')
const webpack3 = require('webpack')

gulp.task('stylesheet', () => {
    return gulp.src(`${config.src}/assets/styles/*.sass`)
    .pipe(_if(config.env_dev, sourcemaps.init()))
    .pipe(sass.sync({
        outputStyle: 'compressed',
        includePaths: ['.']
    }).on('error', sass.logError))
    .pipe(autoprefixer({ browsers: ['last 2 versions']} ))
    .pipe(replace('.--', '.\\--'))
    .pipe(_if(config.env_dev, sourcemaps.write()))
    .pipe(_if(config.env_dev, gulp.dest('.tmp/assets/styles/'), gulp.dest(`${config.dist}/assets/styles/`)))
    .pipe(reload({stream: true}))
});

gulp.task('scripts', () => {
    return gulp.src(`${config.src}/assets/scripts/main.js`)
    .pipe(webpackStream( require('./webpack.config.js')(config.env_dev), webpack3 ))
    .pipe(_if(config.env_dev, gulp.dest('.tmp/assets/scripts/'), gulp.dest(`${config.dist}/assets/scripts/`)))
    .pipe(reload({stream: true}))
});

gulp.task('html', ['stylesheet', 'scripts', 'images'], () => {
    return gulp.src(`${config.src}/index.html`)
    .pipe(useref({searchPath: ['.tmp', config.src, '.']}))
    .pipe(_if('*.js', uglify()))
    .pipe(_if('*.css', cssnano({safe: true, autoprefixer: false})))
    .pipe(gulp.dest(config.dist));
});

gulp.task('fonts', () => {
    return gulp.src([
      'bower_components/**/*.{eot,svg,ttf,woff,woff2}'
    ].concat(`${config.src}/assets/fonts/**/*`))
    .pipe(flatten())
    .pipe(_if(config.env_dev, gulp.dest('.tmp/assets/fonts'), gulp.dest(`${config.dist}/assets/fonts`)))
});

gulp.task('images', () => {
  return gulp.src(`${config.src}/assets/images/**/*`)
    .pipe(imagemin())
    .pipe(gulp.dest(`${config.dist}/assets/images`));
});

gulp.task('clean', del.bind(null, ['.tmp', config.dist]));

gulp.task('serve', ['stylesheet', 'scripts', 'fonts'], () => {
    browserSync.init({
        notify: false,
        port: 9000,
        server: {
            baseDir: ['.tmp', config.src],
            routes: {
                '/scripts': '.tmp/assets/scripts',
                '/bower_components': 'bower_components'
            }
        }
    })
    
    gulp.watch([
        `${config.src}/*.html`,
    ]).on('change', reload);
    
    gulp.watch(`${config.src}/assets/styles/**/*.sass`, ['stylesheet']);
    gulp.watch([`${config.src}/assets/scripts/**/*.js`], ['scripts']);
})

gulp.task('default', () => {
    return new Promise(resolve => {
        config.env_dev = false
        runSequence(['clean'], 'html', 'fonts', resolve)
    });
});