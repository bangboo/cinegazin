const path = require('path')

module.exports = function(dev) {
    const config = {
        module: {
            rules: [{
                test: /.js?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                }
            }]
        },

        output: {
            filename: 'bundled.js'
        }
    }

    if(dev) {
        config.devtool = 'source-maps'
    }

    return config
}