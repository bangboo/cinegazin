import Vue from 'vue/dist/vue.js'

$(document).on('click', '*[data-scroll-to^="#"]', function(e) {
    var id = $(this).attr('data-scroll-to');

    var $id = $(id);
    if ($id.length === 0) {
        return;
    }

    e.preventDefault();

    var pos = $id.offset().top;

    $('body, html').animate({scrollTop: pos});
});

let app = new Vue({
    el: '#app',

    data: {
        cidade: '',
        linkFB: '',

        todos: {},
        items: {},

        overlay: false
    },

    watch: {
        overlay: (value) => {
            if(value) {
                document.querySelector('body').classList.add('overflow')
            } else {
                document.querySelector('body').classList.remove('overflow')
            }
        }
    },

    methods: {
        changeCidade(cidade) {
            const filtrado = this.todos.filter(item => item.cidade === cidade)

            if(filtrado.length < 1) {
                filtrado[0] = this.todos[0]
            }
            
            this.$set(this, 'items', filtrado[0])
            this.$set(this, 'overlay', false)
        },

        teste(fotos) {
            this.$set(this, 'linkFB', fotos)
            setTimeout(() => { FB.XFBML.parse( document.getElementById('modal') ) })

        }
    },

    created() {
        this.$set(this, 'todos', window.cineProgramacao)
        this.changeCidade(window.nextCidadeProgramacao)
    }
})